require 'yaml'

class Perisher
  # Don't look at sun, it burns
  # Also please, use fire extinguisher
  def initialize
    @@borya ||= []
    @@vybegallo ||= seed_vybegallo
  end

  def call
    text = @@vybegallo.sample
    random_randomizer(text)
    text
  end

  private

  def random_randomizer(phrase)
    @@borya << @@vybegallo.delete(phrase)
    if @@vybegallo.size == 0
      @@vybegallo = @@borya
      @@borya = []
    end
  end

  def seed_vybegallo
    origin_phrases = YAML::load_file(File.join(__dir__, 'phrases.yml'))['ruby']
    phrases = origin_phrases['standalone']
    origin_phrases['compose'].each_pair do |phrase, variables|
      variables.each do |zina|
        phrases << phrase.sub('{var}', zina)
      end
    end
    phrases
  end
end
