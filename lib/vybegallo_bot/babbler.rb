class Babbler
  def initialize
    @@line ||= 0
  end

  def call
    @@line += 1
    phrases = YAML::load_file(File.join(__dir__, 'phrases.yml'))['random'][heap]
    phrases.shuffle.sample
  end

  private

  def heap
    (@@line % 3) == 0 ? 'standalone' : 'vybegallo'
  end
end